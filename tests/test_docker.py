import pytest
import requests
import base64
import json
from tensorflow.keras.datasets.cifar100 import load_data
from tensorflow.keras.utils import to_categorical
import numpy as np

#load MNIST dataset
(_, _), (x_test, y_test) = load_data()
# reshape data to have a single channel
x_test = x_test.reshape((x_test.shape[0], x_test.shape[1], x_test.shape[2],x_test.shape[3]))
# normalize pixel values
x_test = x_test/ 255.0
y_test=to_categorical(y_test,100)
#server URL
#url = 'http://ca2-2b04-p2123079-ryanang-main:8501/v1/models/vgg_img_classifier:predict' #see [B]
url = 'https://ca2-dlmodelapp-2rbd.onrender.com/v1/models/vgg_img_classifier:predict' #see [B]

def make_prediction(instances):
    data = json.dumps({"signature_name": "serving_default",
    "instances": instances.tolist()}) #see [C]
    headers = {"content-type": "application/json"}
    json_response = requests.post(url, data=data, headers=headers)
    predictions = json.loads(json_response.text)['predictions']
    return predictions
    
def test_prediction():
    predictions = make_prediction(x_test[0:4]) #see [A]
    for i, pred in enumerate(predictions):
        isinstance(np.argmax(pred), list) #see [D]