#1: Import libraries need for the test
from application.models import Entry
import datetime as datetime
import pytest
from flask import json, requests
import numpy as np

#Unit Test
#2: Parametrize section contains the data for the test
@pytest.mark.parametrize("entrylist",[
    [11,], #Test integer arguments
])
#3: Write the test function pass in the arguments
def test_EntryClass(entrylist,capsys):
    with capsys.disabled():
        print(entrylist)
        now = datetime.datetime.utcnow()
        new_entry = Entry(
                    label = entrylist[0],
                    predicted_on  = now)
        assert new_entry.label == entrylist[0]
        assert new_entry.predicted_on == now

@pytest.mark.xfail(reason="arguments <= 0")
@pytest.mark.parametrize("entrylist",[
    [ 1, 2, 1, 1, 0, 1, 1, 1, 1, 1, -1, 1],
    [ 1, 2, 1, 1, 0, 1, 1, -1, 1, 1, 1, 1],
    [ 1, -2, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [ 1, 2, 1, 1, 0, 1, -1, 1, 1, 1, 1, 1],
    [ 1, 2, 1, 1, 0, 1, -1, 1, 1, 1, 1, 1],
])

def test_EntryValidation(entrylist, capsys):
    test_EntryClass(entrylist, capsys)
'''
def predictA(instance):
    url = 'http://127.0.0.1:5000/predict'
    data = json.dump({'signature_name':'serving_default',
    'instances': instance.tolist()})
    headers = {'content-type': 'application/json'}
    json_response = requests.post(url,data=data,headers=headers)
    predictions = json.loads(json_response.text)['prediction']
    return predictions

def testA_prediction():
    prediction = predictA(X_test[0:4])
    for i,pred in enumerate(predictions):
        assert y_test[i] == np.argmax(pred)'''