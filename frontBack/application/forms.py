from flask_wtf import FlaskForm
from wtforms import FloatField, SubmitField, SelectField
from wtforms.validators import Length, InputRequired, ValidationError, NumberRange


class PastebinEntry(FlaskForm):
    language = SelectField('Models', choices=[('BaseLine', 'BaseLine'), ('VGG Model', 'VGG Model')])