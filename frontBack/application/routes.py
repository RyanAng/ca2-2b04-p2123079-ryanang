from application import app
from flask import render_template, request, flash
from flask_cors import CORS, cross_origin
from tensorflow.keras.preprocessing import image
from PIL import Image, ImageOps
import numpy as np
import tensorflow.keras.models
import re
import base64
from io import BytesIO
import json
import numpy as np
import requests
import pathlib, os
from application import db
from application.models import Entry
from datetime import datetime

def make_prediction(instances):
    data = json.dumps({"signature_name": "serving_default", "instances":
    instances.tolist()})
    headers = {"content-type": "application/json"}
    json_response = requests.post(url, data=data, headers=headers)
    predictions = json.loads(json_response.text)['predictions']
    return predictions

#Server URL – change xyz to Practical 7 deployed URL [TAKE NOTE]
url = 'https://ca2-dlmodelapp-2rbd.onrender.com/v1/models/img_classifier:predict'

#Handles http://127.0.0.1:5000/
@app.route('/')
@app.route('/index')
@app.route('/home')
def index_page():
    return render_template('upload_image.html',entries = get_entries())

def make_prediction(instances):
    data = json.dumps({"signature_name": "serving_default", "instances":
    instances.tolist()})
    headers = {"content-type": "application/json"}
    json_response = requests.post(url, data=data, headers=headers)
    predictions = json.loads(json_response.text)['predictions']
    return predictions


#Handles http://127.0.0.1:5000/predict
@app.route("/predict", methods=['GET','POST'])
@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
def predict():
    try:
        # get data from drawing canvas and save as image
        # Decoding and pre-processing base64 image
        f = request.files['file']
        f.save('output.png')
        img = image.img_to_array(image.load_img("output.png", color_mode="rgb",
        target_size=(32, 32,3))) / 255
        # reshape data to have a single channel
        img = img.reshape(1,32,32,3)
        predictions = make_prediction(img)
        print(predictions)
        ret = ""
        for i, pred in enumerate(predictions):
            ret = "{}".format(np.argmax(pred))
            response = ret
            print(response)
        new_entry = Entry(label = int(response),
                            predicted_on = datetime.utcnow())
        add_entry(new_entry)
        return render_template('upload_image.html',result = response,entries = get_entries())
    except Exception as error:
        flash(error, 'danger')
        return render_template('upload_image.html',result = 'unknow value inserted')
def add_entry(new_entry):
    try:
        db.session.add(new_entry)
        db.session.commit()
        return new_entry.id
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")

def get_entries():
    try:
        # entries = Entry.query.all() # version 2
        entries = db.session.execute(db.select(Entry).order_by(Entry.id)).scalars()
        return entries
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")
        return 0

def remove_entry(id):
    try:
        # entry = Entry.query.get(id) # version 2
        entry = db.get_or_404(Entry, id)
        db.session.delete(entry)
        db.session.commit()
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")
        return 0

@app.route('/remove', methods=['POST'])
def remove():
    req = request.form
    id = req["id"]
    remove_entry(id)
    return render_template('upload_image.html',entries = get_entries())