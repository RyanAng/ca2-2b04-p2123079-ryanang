from application import db

class Entry(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    label = db.Column(db.Integer, nullable=False)
    predicted_on = db.Column(db.DateTime, nullable=False)
