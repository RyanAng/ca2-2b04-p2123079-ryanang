function show_alert(predicted_class){
    alert("Processing Finished.\nPredicted class is *"+predicted_class+"*.")
    }

$("#predictButton").click(function(){
    $('#result').text(' Predicting...');
    var img = canvas.toDataURL('image/png');
    $.ajax({
        type: "POST",
        //url: "http://localhost:5000/predict",
        url: "https://ca2-dlmodelapp-2rbd.onrender.com/predict",
        data: img,
        success: function(data){
            $('#result').text('Predicted Output: ' + data);
        }
    });
});