FROM tensorflow/serving
COPY / /
# RUN apt-get -y update
# && apt-get install -y git && git reset --hard
ENV MODEL_BASE_PATH=/
RUN echo '#!/bin/bash \n\n\
tensorflow_model_server \
--port=8500 \
--rest_api_port=8501 \
--model_config_file=/config/models.conf\
"$@"' > /usr/bin/tf_serving_entrypoint.sh \
&& chmod +x /usr/bin/tf_serving_entrypoint.sh